package com.neuronride.match.features.home.adapter

import android.view.ViewGroup
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.match.features.base.BasePagedListAdapter
import com.neuronride.match.features.base.BaseViewHolder
import com.neuronride.match.features.home.adapter.ViewType.MOVIE_ITEM

// If Later is needed you can use as many types as your requirement needs
private enum class ViewType(val value: Int) {

    MOVIE_ITEM(value = 0)
}

class HomeMoviePagedAdapter(
    clickListener: OnAdapterRowChanges<Movie>
) : BasePagedListAdapter<Movie>(clickListener, HomeMovieDiffCallback) {

    override fun getItemViewType(position: Int): Int = MOVIE_ITEM.value

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        return when (viewType) {
            MOVIE_ITEM.value -> HomeMovieViewHolder(parent)
            else -> throw NotImplementedError()
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        when (holder) {
            is HomeMovieViewHolder -> {
                getItem(position)?.let {
                    holder.bind(
                        item = it,
                        callback = this
                    )
                }
            }
            else -> throw NotImplementedError()
        }
    }
}