package com.neuronride.match.features.home.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.imdb.framework.domain.movies.MoviesService

class HomeDataSourceFactory(
    moviesService: MoviesService,
    searchTerm: String,
    failureLiveData: MutableLiveData<Throwable?>
) : DataSource.Factory<String, Movie>() {

    private val dataSourceLiveData = MutableLiveData<HomeDataSource>()
    private val dataSource = HomeDataSource(moviesService, searchTerm, failureLiveData)

    override fun create(): DataSource<String, Movie> {
        dataSourceLiveData.postValue(dataSource)
        return dataSource
    }

    fun clear() {
        dataSource.clear()
    }
}