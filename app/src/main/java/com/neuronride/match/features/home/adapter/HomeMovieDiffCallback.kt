package com.neuronride.match.features.home.adapter

import androidx.recyclerview.widget.DiffUtil
import com.neuronride.imdb.framework.data.model.Movie

object HomeMovieDiffCallback : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldMovie: Movie, newMovie: Movie) =
        oldMovie.id == newMovie.id

    override fun areContentsTheSame(oldMovie: Movie, newMovie: Movie) =
        oldMovie.id == newMovie.id
}
