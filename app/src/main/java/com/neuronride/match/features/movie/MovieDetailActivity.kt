package com.neuronride.match.features.movie

import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.match.COMPONENT
import com.neuronride.match.R
import com.neuronride.match.features.base.BaseActivity
import com.neuronride.match.util.setImageCacheStrategy
import kotlinx.android.synthetic.main.activity_home.progressBar
import kotlinx.android.synthetic.main.activity_movie_detail.castDivider
import kotlinx.android.synthetic.main.activity_movie_detail.castTextView
import kotlinx.android.synthetic.main.activity_movie_detail.directorDivider
import kotlinx.android.synthetic.main.activity_movie_detail.directorTextView
import kotlinx.android.synthetic.main.activity_movie_detail.genreTextView
import kotlinx.android.synthetic.main.activity_movie_detail.posterImageView
import kotlinx.android.synthetic.main.activity_movie_detail.summaryDivider
import kotlinx.android.synthetic.main.activity_movie_detail.summaryTextView
import kotlinx.android.synthetic.main.activity_movie_detail.titleTextView
import kotlinx.android.synthetic.main.activity_movie_detail.toolbar
import javax.inject.Inject

class MovieDetailActivity : BaseActivity() {

    companion object {
        const val MOVIE_TITLE_TAG = "MovieDetailedActivity:MovieTitle"
        const val MOVIE_ID_TAG = "MovieDetailedActivity:MovieId"
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: MovieDetailViewModel

    private var movieId: String? = null
    private var title: String? = null

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(MOVIE_ID_TAG, movieId)
        outState.putString(MOVIE_TITLE_TAG, title)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_movie_detail)

        COMPONENT.inject(this)

        viewModel = ViewModelProviders.of(
            this,
            viewModelFactory
        )[MovieDetailViewModel::class.java]

        movieId = savedInstanceState?.getString(MOVIE_ID_TAG)
            ?: intent.getStringExtra(MOVIE_ID_TAG)

        title = savedInstanceState?.getString(MOVIE_TITLE_TAG)
            ?: intent.getStringExtra(MOVIE_TITLE_TAG)

        if (movieId == null || title == null) {
            throw NullPointerException("movie id and movie title must be supplied")
        }

        initToolbar()
        titleTextView.text = title
        initMovieObservables()
    }

    private fun initMovieObservables() {

        movieId?.let { id ->
            viewModel.movieLiveData.observe(this, Observer<Movie>
            { movie ->
                progressBar.visibility = View.GONE
                posterImageView.setImageCacheStrategy(Uri.parse(movie.poster))
                summaryTextView.text = movie.plot
                summaryDivider.visibility = View.VISIBLE
                directorTextView.text = getString(
                    R.string.activity_movie_detail_director,
                    movie.director
                )
                directorDivider.visibility = View.VISIBLE
                castTextView.text = getString(
                    R.string.activity_movie_detail_cast,
                    movie.actors
                )
                castDivider.visibility = View.VISIBLE
                genreTextView.text = getString(
                    R.string.activity_movie_detail_genre,
                    movie.genre
                )
            })
            viewModel.movieFailureLiveData.observe(this, Observer {
                progressBar.visibility = View.GONE

                Toast.makeText(
                    this,
                    R.string.activity_movie_detail_data_loading_error,
                    Toast.LENGTH_LONG
                ).show()
            })

            viewModel.searchMovieById(id)
        }
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = title
    }

    private fun initMovieDetail() {
    }
}