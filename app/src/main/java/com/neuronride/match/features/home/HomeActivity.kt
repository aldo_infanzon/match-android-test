package com.neuronride.match.features.home

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.match.COMPONENT
import com.neuronride.match.R
import com.neuronride.match.features.base.BaseActivity
import com.neuronride.match.features.base.BasePagedListAdapter.OnAdapterRowChanges
import com.neuronride.match.features.home.adapter.HomeMoviePagedAdapter
import com.neuronride.match.features.movie.MovieDetailActivity
import kotlinx.android.synthetic.main.activity_home.errorTextView
import kotlinx.android.synthetic.main.activity_home.progressBar
import kotlinx.android.synthetic.main.activity_home.recyclerView
import kotlinx.android.synthetic.main.activity_home.toolbar
import javax.inject.Inject

class HomeActivity : BaseActivity(), OnAdapterRowChanges<Movie> {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var viewModel: HomeViewModel
    private lateinit var adapter: HomeMoviePagedAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_home)

        COMPONENT.inject(this)

        viewModel = ViewModelProviders.of(
            this,
            viewModelFactory
        )[HomeViewModel::class.java]

        initToolbar()
        initAdapter()
    }

    // To Handle clicks coming from the view holder inside the adapter
    override fun onRowClick(view: View, position: Int, item: Movie) {
        val intent = Intent(this, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.MOVIE_ID_TAG, item.id)
        intent.putExtra(MovieDetailActivity.MOVIE_TITLE_TAG, item.title)
        startActivity(intent)
    }

    private fun initAdapter() {
        adapter = HomeMoviePagedAdapter(this)
        recyclerView.adapter = adapter
        viewModel.moviesLiveData.observe(this, Observer<PagedList<Movie>>
        { pagedList ->
            adapter.submitList(pagedList)
        })
        viewModel.moviesFailureLiveData.observe(this, Observer { throwable ->
            progressBar.visibility = View.GONE
            throwable?.let {
                errorTextView.visibility = View.VISIBLE
            }
        })
    }

    private fun initToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.title = getString(R.string.activity_home_title)
        }
    }
}