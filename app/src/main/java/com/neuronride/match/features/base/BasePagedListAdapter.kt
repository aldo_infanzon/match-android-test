package com.neuronride.match.features.base

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import com.neuronride.match.features.base.BaseViewHolder.RowCallback

abstract class BasePagedListAdapter<T>(
    private val clickListener: OnAdapterRowChanges<T>?,
    diffCallback: DiffUtil.ItemCallback<T>
) : PagedListAdapter<T, BaseViewHolder<*>>(diffCallback), RowCallback<T> {

    override fun getItemViewType(position: Int) = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        throw NotImplementedError()
    }

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        throw NotImplementedError()
    }

    override fun rowClick(view: View, position: Int, item: T) {
        clickListener?.onRowClick(view, position, item)
    }

    interface OnAdapterRowChanges<T> {
        fun onRowClick(view: View, position: Int, item: T)
    }
}