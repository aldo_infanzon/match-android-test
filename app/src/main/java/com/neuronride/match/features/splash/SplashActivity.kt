package com.neuronride.match.features.splash

import android.content.Intent
import android.os.Bundle
import com.neuronride.match.features.base.BaseActivity
import com.neuronride.match.features.home.HomeActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }
}