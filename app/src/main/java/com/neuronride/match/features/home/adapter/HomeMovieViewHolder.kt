package com.neuronride.match.features.home.adapter

import android.net.Uri
import android.view.ViewGroup
import com.jakewharton.rxbinding3.view.clicks
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.match.R
import com.neuronride.match.features.base.BaseViewHolder
import com.neuronride.match.util.CLICK_DELAY_MILLISECONDS
import com.neuronride.match.util.setImageCacheStrategy
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import kotlinx.android.synthetic.main.view_row_movie_item.view.posterImageView
import kotlinx.android.synthetic.main.view_row_movie_item.view.titleTextView
import kotlinx.android.synthetic.main.view_row_movie_item.view.yearTextView
import java.util.concurrent.TimeUnit

internal class HomeMovieViewHolder(
    parent: ViewGroup
) : BaseViewHolder<Movie>(
    parent,
    R.layout.view_row_movie_item
) {

    private var rowClickDisposable = Disposables.disposed()

    override lateinit var item: Movie

    override fun clean() {
        rowClickDisposable.dispose()
    }

    override fun bind(item: Movie, callback: RowCallback<Movie>) {
        this.item = item

        itemView.titleTextView.text = this.item.title
        itemView.yearTextView.text = this.item.year
        itemView.posterImageView.setImageCacheStrategy(Uri.parse(this.item.poster))

        rowClickDisposable = itemView.clicks()
            .throttleFirst(CLICK_DELAY_MILLISECONDS, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                callback.rowClick(itemView, adapterPosition, this.item)
            }
    }
}