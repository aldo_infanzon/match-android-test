package com.neuronride.match.features.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.imdb.framework.domain.movies.MoviesService
import com.neuronride.match.features.home.datasource.HomeDataSourceFactory
import javax.inject.Inject

private const val PAGE_SIZE = 10

class HomeViewModel @Inject constructor(
    service: MoviesService
) : ViewModel() {

    val moviesLiveData: LiveData<PagedList<Movie>>
    val moviesFailureLiveData = MutableLiveData<Throwable?>()

    // This is only intended for testing purposes if needed replace with
    // a search box (Edit text or similar)
    private val searchTerm = "Star Wars"
    private val sourceFactory: HomeDataSourceFactory =
        HomeDataSourceFactory(service, searchTerm, moviesFailureLiveData)

    init {
        moviesLiveData = LivePagedListBuilder<String, Movie>(
            sourceFactory,
            PagedList.Config.Builder()
                .setPageSize(PAGE_SIZE)
                .setEnablePlaceholders(false)
                .build()
        ).build()
    }

    override fun onCleared() {
        super.onCleared()
        sourceFactory.clear()
    }
}