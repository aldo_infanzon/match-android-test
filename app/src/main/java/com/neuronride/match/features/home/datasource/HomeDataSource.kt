package com.neuronride.match.features.home.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.ItemKeyedDataSource
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.imdb.framework.domain.movies.MoviesService
import com.neuronride.match.util.defaultSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import timber.log.Timber

private const val API_PAGE_SIZE = 10

class HomeDataSource(
    private val moviesService: MoviesService,
    private val searchTerm: String,
    // Simple failure data if needed create a model to hold more information about the failure
    private val failureLiveData: MutableLiveData<Throwable?>
) : ItemKeyedDataSource<String, Movie>() {

    private var page = 1
    private var remainingResults = 0
    private val itemsSet = HashSet<String>()
    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun loadInitial(
        params: LoadInitialParams<String>,
        callback: LoadInitialCallback<Movie>
    ) {
        moviesService.searchMovie(searchTerm, page)
            .defaultSchedulers()
            .subscribe({ listResults ->
                // This results from the api sometimes contains some repeated items
                // so we get rid of them first and then we check remaining items
                val list: MutableList<Movie> = removeRepeatedItems(listResults.results)
                remainingResults = listResults.totalResults - list.size
                remainingResults -= (API_PAGE_SIZE - list.size) // Adjustment when repeated items are found
                // Always increase the current page until we got the results
                // Otherwise on failures it will request the incorrect page
                ++page
                callback.onResult(list)
                failureLiveData.postValue(null)
            }, { throwable ->
                Timber.e(throwable)
                failureLiveData.postValue(throwable)
            })
            .addTo(disposables)
    }

    override fun loadAfter(params: LoadParams<String>, callback: LoadCallback<Movie>) {
        // Only load items if there a remaining results
        if (remainingResults > 0) {
            moviesService.searchMovie(searchTerm, page)
                .defaultSchedulers()
                .subscribe({ listResults ->
                    // This results from the api sometimes contains some repeated items
                    // so we get rid of them first and then we check remaining items
                    val list: MutableList<Movie> = removeRepeatedItems(listResults.results)
                    remainingResults -= list.size
                    remainingResults -= (API_PAGE_SIZE - list.size) // Adjustment when repeated items are found)
                    // Always increase the current page until we got the results
                    // Otherwise on failures it will request the incorrect page
                    ++page
                    callback.onResult(list)
                }, { throwable ->
                    Timber.e(throwable)
                })
                .addTo(disposables)
        }
    }

    override fun loadBefore(params: LoadParams<String>, callback: LoadCallback<Movie>) {
        /// Not needed so we left this empty
    }

    override fun getKey(item: Movie): String = item.id

    fun clear() {
        disposables.clear()
    }

    private fun removeRepeatedItems(list: List<Movie>): MutableList<Movie> {
        val newList: MutableList<Movie> = mutableListOf()
        list.forEach { movie ->
            if (!itemsSet.contains(movie.id)) {
                newList.add(movie)
                itemsSet.add(movie.id)
            }
        }
        return newList
    }
}