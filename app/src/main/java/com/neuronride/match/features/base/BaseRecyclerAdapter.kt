package com.neuronride.match.features.base

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<T>(
    private val itemList: MutableList<T>,
    private val clickListener: OnAdapterRowChanges<T>?

) : RecyclerView.Adapter<BaseViewHolder<*>>(),
    BaseViewHolder.RowCallback<T> {

    override fun getItemViewType(position: Int) = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<*> {
        throw NotImplementedError()
    }

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: BaseViewHolder<*>, position: Int) {
        throw NotImplementedError()
    }

    override fun rowClick(view: View, position: Int, item: T) {
        clickListener?.onRowClick(view, position, item)
    }

    open fun setItems(itemList: List<T>) {
        this.itemList.clear()
        this.itemList.addAll(itemList)
    }

    interface OnAdapterRowChanges<T> {
        fun onRowClick(view: View, position: Int, item: T)
    }
}