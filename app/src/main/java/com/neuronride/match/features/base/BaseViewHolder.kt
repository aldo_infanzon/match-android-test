package com.neuronride.match.features.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T>(

    parent: ViewGroup,

    @LayoutRes layoutId: Int

) : RecyclerView.ViewHolder(

    LayoutInflater.from(parent.context)
        .inflate(layoutId, parent, false)
) {

    abstract var item: T

    /**
     * Use this to clean any object from memory. Is recommended to use this on onViewRecycled
     */
    abstract fun clean()

    abstract fun bind(item: T, callback: RowCallback<T>)

    interface RowCallback<T> {

        fun rowClick(view: View, position: Int, item: T)
    }
}