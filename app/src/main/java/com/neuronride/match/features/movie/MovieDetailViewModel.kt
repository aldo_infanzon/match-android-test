package com.neuronride.match.features.movie

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.imdb.framework.domain.movies.MoviesService
import com.neuronride.match.util.defaultSchedulers
import io.reactivex.disposables.Disposables
import timber.log.Timber
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(
    private val service: MoviesService
) : ViewModel() {

    var movie: Movie? = null
    val movieLiveData = MutableLiveData<Movie>()
    val movieFailureLiveData = MutableLiveData<Throwable>()

    private var movieDisposable = Disposables.disposed()

    override fun onCleared() {
        movieDisposable.dispose()
        super.onCleared()
    }

    fun searchMovieById(id: String) {
        movie?.let {
            movieLiveData.postValue(movie)
        } ?: run {
            if (movieDisposable.isDisposed) { // Only called when is disposed
                movieDisposable = service.fetchMovieById(id)
                    .doOnNext { service.saveMovie(it).blockingGet() }
                    // Uses timeout otherwise waits for a result
                    .onErrorResumeNext(service.getMovieById(id).toFlowable())
                    .defaultSchedulers()
                    .subscribe({
                        movie = it
                        movieLiveData.postValue(movie)
                    }, { throwable ->
                        Timber.e(throwable)
                        movieFailureLiveData.postValue(throwable)
                    })
            }
        }
    }
}