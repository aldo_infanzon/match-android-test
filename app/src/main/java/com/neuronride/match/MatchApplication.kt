package com.neuronride.match

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import com.neuronride.match.di.component.ApplicationComponent
import com.neuronride.match.di.component.DaggerApplicationComponent
import com.neuronride.match.di.module.ApplicationModule
import timber.log.Timber
import timber.log.Timber.DebugTree

lateinit var COMPONENT: ApplicationComponent

class MatchApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(DebugTree())
        }

        Fresco.initialize(this)

        COMPONENT = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
    }
}