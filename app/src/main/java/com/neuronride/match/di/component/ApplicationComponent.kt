package com.neuronride.match.di.component

import com.neuronride.match.di.module.ApplicationModule
import com.neuronride.match.di.module.FrameworkModule
import com.neuronride.match.di.module.ViewModelModule
import com.neuronride.match.features.home.HomeActivity
import com.neuronride.match.features.movie.MovieDetailActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        FrameworkModule::class,
        ViewModelModule::class
    ]
)
interface ApplicationComponent {

    fun inject(target: HomeActivity)

    fun inject(target: MovieDetailActivity)
}