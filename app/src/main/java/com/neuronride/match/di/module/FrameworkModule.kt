package com.neuronride.match.di.module

import android.app.Application
import com.neuronride.imdb.framework.ImdbFramework
import com.neuronride.imdb.framework.MoviesFramework
import com.neuronride.imdb.framework.domain.movies.MoviesService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FrameworkModule {

    @Singleton
    @Provides
    fun provideImdbFramework(
        application: Application
    ): MoviesFramework =
        ImdbFramework.init(application)

    @Provides
    fun provideMoviesService(
        moviesFramework: MoviesFramework
    ): MoviesService = moviesFramework.moviesService
}