package com.neuronride.match.util

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

fun <T> Observable<T>.defaultSchedulers(): Observable<T> =
    subscribeOn(ioProvider)
        .observeOn(uiProvider)

fun <T> Single<T>.defaultSchedulers(): Single<T> =
    subscribeOn(ioProvider).observeOn(uiProvider)

fun Completable.defaultSchedulers(): Completable =
    subscribeOn(ioProvider).observeOn(uiProvider)

fun <T> Maybe<T>.defaultSchedulers(): Maybe<T> =
    subscribeOn(ioProvider).observeOn(uiProvider)

fun <T> Flowable<T>.defaultSchedulers(): Flowable<T> =
    subscribeOn(ioProvider).observeOn(uiProvider)

private val uiProvider: Scheduler = AndroidSchedulers.mainThread()

private val ioProvider: Scheduler = Schedulers.io()
