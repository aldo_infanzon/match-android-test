package com.neuronride.imdb

import androidx.room.EmptyResultSetException
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.imdb.framework.repository.DefaultDatabase
import com.neuronride.imdb.framework.repository.ImdbMovie
import com.neuronride.imdb.framework.repository.ImdbMovieDao
import org.junit.*
import org.junit.runner.*

val firstMovie: Movie = ImdbMovie(
    id = "DummyId1",
    year = "",
    writer = "",
    website = "",
    type = "",
    runtime = "",
    released = "",
    rated = "",
    production = "",
    poster = "",
    plot = "",
    language = "",
    genre = "",
    director = "",
    country = "",
    boxOffice = "",
    awards = "",
    actors = "",
    title = ""
)

@RunWith(AndroidJUnit4ClassRunner::class)
class DbTest {

    private lateinit var dao: ImdbMovieDao
    private lateinit var db: DefaultDatabase
    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    @Before
    fun createDb() {

        db = DefaultDatabase.getInstance(context)
        dao = db.movieDao()
    }

    @Test
    @Throws(Exception::class)
    fun writeMovieAndReadMovie() {
        dao.insert(firstMovie as ImdbMovie)
            .test()
            .assertComplete()

        dao.findById(firstMovie.id)
            .test()
            .assertNoErrors()

        dao.findById("OtherId")
            .test()
            .assertError(EmptyResultSetException::class.java)
    }
}