package com.neuronride.imdb.framework.di.module

import android.content.Context
import com.neuronride.imdb.framework.repository.DefaultDatabase
import com.neuronride.imdb.framework.repository.ImdbMovieDao
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {

    @Provides
    fun providesDatabase(
        context: Context
    ): DefaultDatabase = DefaultDatabase.getInstance(context)

    @Provides
    fun provideImdbDao(
        database: DefaultDatabase
    ): ImdbMovieDao = database.movieDao()
}