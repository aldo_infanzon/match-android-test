package com.neuronride.imdb.framework.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

private const val VERSION = 1
private const val NAME = "repository-imdb-database"

@Database(
    entities = [
        ImdbMovie::class
    ],
    version = VERSION
)
abstract class DefaultDatabase : RoomDatabase() {

    companion object {

        @Volatile
        private var INSTANCE: DefaultDatabase? = null

        fun getInstance(context: Context): DefaultDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { db ->
                    INSTANCE = db
                }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                DefaultDatabase::class.java, NAME
            ).build()
    }

    abstract fun movieDao(): ImdbMovieDao
}