package com.neuronride.imdb.framework.di.component

import com.neuronride.imdb.framework.ImdbFramework
import com.neuronride.imdb.framework.di.module.GatewayModule
import com.neuronride.imdb.framework.di.module.LibraryModule
import com.neuronride.imdb.framework.di.module.RepositoryModule
import com.neuronride.imdb.framework.di.module.ServiceModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        LibraryModule::class,
        ServiceModule::class,
        RepositoryModule::class,
        GatewayModule::class
    ]
)
interface LibraryComponent {

    fun inject(target: ImdbFramework)
}