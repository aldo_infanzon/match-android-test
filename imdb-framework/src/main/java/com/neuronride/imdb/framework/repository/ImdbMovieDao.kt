package com.neuronride.imdb.framework.repository

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface ImdbMovieDao {

    @Query("SELECT * from movie_table WHERE id = :id")
    fun findById(id: String): Single<ImdbMovie>

    /**
     * Insert an item in the database.
     *
     * @param item the obj to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: ImdbMovie): Completable

    @Query("DELETE FROM movie_table")
    fun deleteAll()
}