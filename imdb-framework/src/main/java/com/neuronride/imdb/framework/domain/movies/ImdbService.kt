package com.neuronride.imdb.framework.domain.movies

import com.neuronride.imdb.framework.data.model.ListResults
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.imdb.framework.data.movies.MoviesGateway
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class ImdbService(
    private val gateway: MoviesGateway<Movie, ListResults<Movie>>
) : MoviesService {

    override fun fetchMovieById(id: String): Flowable<Movie> {
        return gateway.fetchMovie(id)
    }

    override fun getMovieById(id: String): Single<Movie> {
        return gateway.getMovie(id)
    }

    override fun saveMovie(movie: Movie): Completable {
        return gateway.createMovie(item = movie)
    }

    override fun searchMovie(searchTerm: String, page: Int): Flowable<ListResults<Movie>> =
        gateway.searchMovie(searchTerm, page)
}