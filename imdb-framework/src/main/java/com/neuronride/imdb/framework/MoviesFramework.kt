package com.neuronride.imdb.framework

import com.neuronride.imdb.framework.domain.movies.MoviesService

interface MoviesFramework {
    var moviesService: MoviesService
}