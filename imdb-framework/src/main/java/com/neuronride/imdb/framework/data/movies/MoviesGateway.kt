package com.neuronride.imdb.framework.data.movies

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface MoviesGateway<T, P> {

    fun searchMovie(term: String, page: Int): Flowable<P>

    fun fetchMovie(id: String): Flowable<T>

    fun getMovie(id: String): Single<T>

    fun createMovie(item: T): Completable

    fun updateMovie(item: T): Completable

    fun deleteMovie(id: String): Completable
}