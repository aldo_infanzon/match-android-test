package com.neuronride.imdb.framework.di.module

import android.app.Application
import android.content.Context
import com.neuronride.imdb.framework.R
import com.neuronride.imdb.framework.data.movies.ImdbSearchApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val TIMEOUT_MILLIS = 15000L
private const val READ_TIMEOUT_MILLIS = 15000L
private const val WRITE_TIMEOUT_MILLIS = 15000L
private const val CALL_TIMEOUT_MILLIS = 30000L

private const val HEADER_HOST = "x-rapidapi-host"
private const val HEADER_KEY = "x-rapidapi-key"

@Module
class LibraryModule(private val application: Application) {

    @Provides
    fun provideContext(): Context = application

    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
            .readTimeout(READ_TIMEOUT_MILLIS, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT_MILLIS, TimeUnit.SECONDS)
            .callTimeout(CALL_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS)
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader(
                        HEADER_HOST,
                        application.getString(R.string.imdb_header_host_value)
                    )
                    .addHeader(
                        HEADER_KEY,
                        application.getString(R.string.imdb_header_key_value)
                    ).build()
                chain.proceed(request)
            }
            .build()

    @Singleton
    @Provides
    fun provideImdbSearchApiRetrofit(client: OkHttpClient): ImdbSearchApi =
        Retrofit.Builder()
            .baseUrl(application.getString(R.string.imdb_base_api_host))
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(ImdbSearchApi::class.java)
}