package com.neuronride.imdb.framework.di.module

import com.neuronride.imdb.framework.data.model.ListResults
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.imdb.framework.data.movies.MoviesGateway
import com.neuronride.imdb.framework.domain.movies.ImdbService
import com.neuronride.imdb.framework.domain.movies.MoviesService
import dagger.Module
import dagger.Provides

@Module
class ServiceModule {

    @Provides
    fun provideImdbService(
        gateway: MoviesGateway<Movie, ListResults<Movie>>
    ): MoviesService = ImdbService(gateway)
}