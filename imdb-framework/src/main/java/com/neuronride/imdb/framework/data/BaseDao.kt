package com.neuronride.imdb.framework.data

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

@Dao
interface BaseDao<T> {


    /**
     * Insert an array of item in the database.
     *
     * @param item the obj to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg item: T)

    /**
     * Update an item from the database.
     *
     * @param item the obj to be updated
     */
    @Update
    fun update(item: T)

    /**
     * Delete an item from the database
     *
     * @param item the obj to be deleted
     */
    @Delete
    fun delete(item: T)
}