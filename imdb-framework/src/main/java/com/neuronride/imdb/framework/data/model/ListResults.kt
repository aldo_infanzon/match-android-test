package com.neuronride.imdb.framework.data.model

interface ListResults<T> {
    val results: List<T>
    val totalResults: Int
}