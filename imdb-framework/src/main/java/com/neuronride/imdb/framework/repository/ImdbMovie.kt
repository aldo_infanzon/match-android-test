package com.neuronride.imdb.framework.repository

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.neuronride.imdb.framework.data.model.Movie

const val TABLE_NAME = "movie_table"

@Entity(tableName = TABLE_NAME)
data class ImdbMovie(
    @PrimaryKey
    override val id: String,
    override val title: String,
    override val year: String,
    override val type: String,
    override val poster: String,
    override val rated: String,
    override val released: String,
    override val runtime: String,
    override val genre: String,
    override val director: String,
    override val writer: String,
    override val actors: String,
    override val plot: String,
    override val language: String,
    override val country: String,
    override val awards: String,
    override val boxOffice: String,
    override val production: String,
    override val website: String
) : Movie