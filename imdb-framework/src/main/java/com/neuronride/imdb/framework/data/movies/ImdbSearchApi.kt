package com.neuronride.imdb.framework.data.movies

import com.google.gson.annotations.SerializedName
import com.neuronride.imdb.framework.data.model.ListResults
import com.neuronride.imdb.framework.data.model.Movie
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Query

private const val JSON_PARAMETER = "?r=json"

private const val SEARCH_PARAMETER = "s"
private const val PAGE_PARAMETER = "page"
private const val ID_PARAMETER = "i"

interface ImdbSearchApi {

    @GET(value = JSON_PARAMETER)
    fun searchMovie(
        @Query(PAGE_PARAMETER) page: Int,
        @Query(SEARCH_PARAMETER) searchText: String
    ): Flowable<MovieListResponse>

    class MovieListResponse(
        @SerializedName("Search")
        override val results: List<ImdbApiMovie>,
        override val totalResults: Int
    ) : ListResults<Movie>

    @GET(value = JSON_PARAMETER)
    fun getMovieById(
        @Query(ID_PARAMETER) id: String
    ): Flowable<ImdbApiMovie>

    class ImdbApiMovie(

        @SerializedName(value = "imdbID")
        override val id: String,

        @SerializedName(value = "Title")
        override val title: String,

        @SerializedName(value = "Year")
        override val year: String,

        @SerializedName(value = "Type")
        override val type: String,

        @SerializedName(value = "Poster")
        override val poster: String,

        @SerializedName(value = "Rated")
        override val rated: String,

        @SerializedName(value = "Genre")
        override val genre: String,

        @SerializedName(value = "Released")
        override val released: String,

        @SerializedName(value = "Runtime")
        override val runtime: String,

        @SerializedName(value = "Director")
        override val director: String,

        @SerializedName(value = "Writer")
        override val writer: String,

        @SerializedName(value = "Actors")
        override val actors: String,

        @SerializedName(value = "Awards")
        override val awards: String,

        @SerializedName(value = "Country")
        override val country: String,

        @SerializedName(value = "Language")
        override val language: String,

        @SerializedName(value = "Plot")
        override val plot: String,

        @SerializedName(value = "BoxOffice")
        override val boxOffice: String,

        @SerializedName(value = "Production")
        override val production: String,

        @SerializedName(value = "Website")
        override val website: String

    ) : Movie
}
