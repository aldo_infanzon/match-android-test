package com.neuronride.imdb.framework.di.module

import com.neuronride.imdb.framework.data.model.ListResults
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.imdb.framework.data.movies.ImdbGateway
import com.neuronride.imdb.framework.data.movies.ImdbSearchApi
import com.neuronride.imdb.framework.data.movies.MoviesGateway
import com.neuronride.imdb.framework.repository.ImdbMovieDao
import dagger.Module
import dagger.Provides

@Module
class GatewayModule {

    @Provides
    fun provideImdbGateway(
        searchApi: ImdbSearchApi,
        dao: ImdbMovieDao
    ): MoviesGateway<Movie, ListResults<Movie>> =
        ImdbGateway(searchApi, dao)
}