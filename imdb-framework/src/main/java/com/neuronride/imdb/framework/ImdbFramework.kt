package com.neuronride.imdb.framework

import android.app.Application
import com.neuronride.imdb.framework.di.component.DaggerLibraryComponent
import com.neuronride.imdb.framework.di.component.LibraryComponent
import com.neuronride.imdb.framework.di.module.LibraryModule
import com.neuronride.imdb.framework.domain.movies.MoviesService
import javax.inject.Inject

private lateinit var COMPONENT: LibraryComponent

class ImdbFramework private constructor() : MoviesFramework {

    companion object {
        fun init(application: Application): ImdbFramework {
            val instance = ImdbFramework()

            COMPONENT = DaggerLibraryComponent.builder()
                .libraryModule(LibraryModule(application))
                .build()

            COMPONENT.inject(instance)

            return instance
        }
    }

    @Inject
    override lateinit var moviesService: MoviesService
}