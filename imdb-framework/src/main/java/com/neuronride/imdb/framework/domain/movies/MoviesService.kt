package com.neuronride.imdb.framework.domain.movies

import com.neuronride.imdb.framework.data.model.ListResults
import com.neuronride.imdb.framework.data.model.Movie
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

interface MoviesService {

    fun fetchMovieById(id: String): Flowable<Movie>

    fun getMovieById(id: String): Single<Movie>

    fun saveMovie(movie: Movie): Completable

    fun searchMovie(searchTerm: String, page: Int): Flowable<ListResults<Movie>>
}