package com.neuronride.imdb.framework.data.model

interface Movie {
    val id: String
    val title: String
    val year: String
    val type: String
    val poster: String
    val rated: String
    val released: String
    val runtime: String
    val genre: String
    val director: String
    val writer: String
    val actors: String
    val plot: String
    val language: String
    val country: String
    val awards: String
    val boxOffice: String
    val production: String
    val website: String
}