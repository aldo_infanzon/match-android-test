package com.neuronride.imdb.framework.data.movies

import com.neuronride.imdb.framework.data.model.ListResults
import com.neuronride.imdb.framework.data.model.Movie
import com.neuronride.imdb.framework.data.movies.ImdbSearchApi.ImdbApiMovie
import com.neuronride.imdb.framework.repository.ImdbMovie
import com.neuronride.imdb.framework.repository.ImdbMovieDao
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class ImdbGateway(
    private val requestInterface: ImdbSearchApi,
    private val dao: ImdbMovieDao
) : MoviesGateway<Movie, ListResults<Movie>> {

    override fun searchMovie(term: String, page: Int): Flowable<ListResults<Movie>> =
        requestInterface.searchMovie(page, term)
            .map { response -> response }

    override fun fetchMovie(id: String): Flowable<Movie> {
        return requestInterface.getMovieById(id)
            .map { apiMovie ->
                mapRequestMovie(apiMovie)
            }
    }

    override fun getMovie(id: String): Single<Movie> = dao.findById(id).map { it }

    override fun createMovie(item: Movie) = dao.insert(item as ImdbMovie)

    override fun updateMovie(item: Movie): Completable {
        throw NotImplementedError()
    }

    override fun deleteMovie(id: String): Completable {
        throw NotImplementedError()
    }

    private fun mapRequestMovie(apiMovie: ImdbApiMovie): ImdbMovie {
        return ImdbMovie(
            id = apiMovie.id,
            title = apiMovie.title,
            actors = apiMovie.actors,
            awards = apiMovie.awards,
            boxOffice = apiMovie.boxOffice,
            country = apiMovie.country,
            director = apiMovie.director,
            genre = apiMovie.genre,
            language = apiMovie.language,
            plot = apiMovie.plot,
            poster = apiMovie.poster,
            production = apiMovie.production,
            rated = apiMovie.rated,
            released = apiMovie.rated,
            runtime = apiMovie.runtime,
            type = apiMovie.type,
            website = apiMovie.website,
            writer = apiMovie.writer,
            year = apiMovie.year
        )
    }
}